from django.shortcuts import render
from .models import *
from rest_framework.response import Response
from rest_framework import permissions
from rest_framework.decorators import permission_classes
from rest_framework.views import APIView
from product.models import Product

class getCategory(APIView):
    permission_classes = [permissions.AllowAny,]

    def get(self, request):
        category = Category.objects.all()
        s = []
        for i in category:
            l = len(Product.objects.filter(category=i))
            s.append({'id': i.id, 'name': i.name, 'count': l})
        return Response(s)
