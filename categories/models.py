from django.db import models
from django.utils.translation import ugettext_lazy as _

class Category(models.Model):
    name = models.CharField(max_length=250)

    class Meta:
        verbose_name = _("Категории")
        verbose_name_plural = _("Категории")

    def __str__(self):
        return self.name