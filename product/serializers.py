from rest_framework import serializers
from .models import *

class PImgSer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField('get_image_url')
    def get_image_url(self, obj):
        return self.context['request'].build_absolute_uri(obj.image.url)
    class Meta:
        model = ProductImage
        fields = ('image',)

class ProductSer(serializers.ModelSerializer):
    product_image = PImgSer(many=True)
    class Meta:
        model = Product
        fields = "__all__"


class idSer(serializers.Serializer):
    id = serializers.IntegerField()


class orderSer(serializers.Serializer):
    name = serializers.CharField()
    phone = serializers.CharField()
    products = serializers.ListField()