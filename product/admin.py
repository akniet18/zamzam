from django.contrib import admin
from .models import *
from django import forms

class PIAdmin(admin.TabularInline):
    model = ProductImage

class ProductAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'category')
    list_filter = ('category', 'age', 'gender')
    inlines = [PIAdmin]
    search_fields = ('name', 'decription')
    exclude = ['count_rec', 'sale_price']

admin.site.register(Product, ProductAdmin)
admin.site.register(Brand)

class OPAdmin(admin.TabularInline):
    model = OrderProduct

class OrderAdmin(admin.ModelAdmin):
    inlines = [OPAdmin]
    search_fields = ('name', 'phone')
    list_display = ('name', 'phone')
    # list_filter = ('category', 'age', 'gender')
    
admin.site.register(Order, OrderAdmin)
