from django.urls import path, include
from .views import *

urlpatterns = [
    path('', getProduct.as_view({'get': 'list'})),
    path('<id>', GetDetailProduct.as_view()),
    path('rec/', recomendation.as_view()),
    path('new/', new_top_product.as_view()),
    path('similar/', similarProduct.as_view()),
    path('order/', toOrder.as_view()),
    path('brands/', getBrands.as_view())
]