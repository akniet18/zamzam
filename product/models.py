from django.db import models
from django.utils.translation import ugettext_lazy as _
from io import BytesIO
from PIL import Image
from django.core.files.uploadedfile import InMemoryUploadedFile
import sys


class Brand(models.Model):
    name = models.CharField(max_length=250)

    class Meta:
        verbose_name = _("Бренды")
        verbose_name_plural = _("Бренды")

    def __str__(self):
        return self.name


class Product(models.Model):
    AGE_ZERO = 0
    AGE_ONE = 1
    AGE_36_M = 2
    # AGE_3 = 3
    # AGE_6 = 4
    AGE_9 = 5
    AGE_16 = 6
    # AGE_18 = 7
    AGE_CHOICES = (
        (AGE_ZERO, '0-12 месяцев'),
        (AGE_ONE, '1-3 года'),
        (AGE_36_M, '4-5 лет'),
        # (AGE_3, '3-5 Years'),
        # (AGE_6, '6-8 лет'),
        (AGE_9, '9-12 лет'),
        (AGE_16, 'болеее 12 лет')
        # (AGE_18, '18+')
    )

    GENDER_FEMALE = 0
    GENDER_MALE = 1
    GENDER_BABE = 2
    GENDER_CHOICES = ((GENDER_FEMALE, 'Девочкам'), (GENDER_MALE, 'Мальчикам'), (GENDER_BABE, 'Малышам'))

    name = models.CharField(_("Название"), max_length=250)
    category = models.ForeignKey("categories.Category", on_delete=models.CASCADE, null=True, blank=True, verbose_name=_("Категории"))
    price = models.BigIntegerField(null=True, blank=True, verbose_name=_("Цена"))
    decription = models.TextField(null=True, blank=True, verbose_name=_("О продукте"))
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE, null=True, blank=True, verbose_name=_("Бренд"))
    sale = models.IntegerField(default=0, blank=True, verbose_name=_("Акция"))
    sale_price = models.BigIntegerField(null=True, blank=True)
    available = models.BooleanField(default=True, verbose_name=_("В наличий"))
    count_rec = models.BigIntegerField(default=0)
    created = models.DateTimeField(auto_now_add=True, blank=True)
    gender = models.SmallIntegerField(choices=GENDER_CHOICES, null=True, blank=True)
    age = models.SmallIntegerField(choices=AGE_CHOICES, null=True, blank=True)
    article = models.CharField(max_length=50, null=True, blank=True, verbose_name=_("Артикль"))
    link = models.CharField(max_length=250, null=True, blank=True)

    class Meta:
        verbose_name = _("Продукты")
        verbose_name_plural = _("Продукты")

    def __str__(self):
        return self.name   

    
    def save(self):
        self.sale_price = self.price - (self.price * self.sale) / 100
        super(Product, self).save()


def product_photos_dir(instanse, filename):
    folder_name = f"{instanse.id}/{filename}"
    return folder_name
class ProductImage(models.Model):
    image = models.ImageField(upload_to=product_photos_dir)
    product = models.ForeignKey("Product", on_delete=models.CASCADE, related_name="product_image")

    def __str__(self):
        return self.product.name

    def save(self):
        # Opening the uploaded image
        im = Image.open(self.image)
        output = BytesIO()
        # Resize/modify the image
        width, height = im.size
        if width > 1000:
            im = im.resize((width-200, height-200))

        # after modifications, save it to the output
        im.save(output, format='JPEG', quality=90)
        output.seek(0)

        # change the imagefield value to be the newley modifed image value
        self.image = InMemoryUploadedFile(output, 'ImageField', "%s.jpg" % self.image.name.split('.')[0], 'image/jpeg',
                                        sys.getsizeof(output), None)

        super(ProductImage, self).save()



class Order(models.Model):
    phone = models.CharField(max_length=50, null=True, blank=True)
    name = models.CharField(max_length=150, null=True, blank=True)
    
    class Meta:
        verbose_name = _("Заказы")
        verbose_name_plural = _("Заказы")

    def __str__(self):
        return self.name
    

class OrderProduct(models.Model):
    product = models.ForeignKey(Product, verbose_name=_("Товар"), on_delete=models.CASCADE)
    count = models.BigIntegerField()
    order = models.ForeignKey(Order, on_delete=models.CASCADE, null=True, blank=True)