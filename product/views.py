from django.shortcuts import render
from rest_framework.views import APIView
from django.shortcuts import get_object_or_404
import random
from .serializers import *
from .models import *
from rest_framework.response import Response
from rest_framework import permissions
from rest_framework.decorators import permission_classes
from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token
from rest_framework import viewsets, generics
from rest_framework import filters
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.pagination import PageNumberPagination # Any other type works as well



class getProduct(viewsets.ModelViewSet):
    permission_classes = [permissions.AllowAny,]
    queryset = Product.objects.all()
    serializer_class = ProductSer
    pagination_class = PageNumberPagination
    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    search_fields = ('name', 'decription')
    filter_fields = ("gender",)
    
    def get_queryset(self):
        minheight = self.request.GET.get('minprice')
        maxheight = self.request.GET.get('maxprice')
        category = self.request.GET.get('category')
        brand = self.request.GET.get('brand')
        age = self.request.GET.get('age')
        if (minheight and maxheight):
            self.queryset = self.queryset.filter(price__gte=minheight, price__lte=maxheight)
        if (category):
            category = category.split(',')
            self.queryset = self.queryset.filter(category__id__in=category)
        if (brand):
            brand = brand.split(',')
            self.queryset = self.queryset.filter(brand__id__in=brand)
        if (age):
            age = age.split(',')
            self.queryset = self.queryset.filter(age__in=age)
        return self.queryset


class GetDetailProduct(APIView):
    permission_classes = [permissions.AllowAny,]

    def get(self, request, id):
        queryset = Product.objects.get(id=id)
        serializer_class = ProductSer(queryset, context={'request': request})
        return Response(serializer_class.data)


class recomendation(APIView):
    permission_classes = [permissions.AllowAny,]

    def get(self, request):
        queryset = Product.objects.order_by('-count_rec')[:8]
        serializer_class = ProductSer(queryset, many = True, context={'request': request})
        return Response(serializer_class.data)

    def post(self, request):
        s = idSer(data=request.data)
        if s.is_valid():
            product = Product.objects.get(id=s.validated_data['id'])
            product.count_rec += 1
            product.save()
            return Response({'status': 'success'})
        else:
            return Response(s.errors)


class new_top_product(APIView):
    permission_classes = [permissions.AllowAny,]

    def get(self, request):
        queryset = Product.objects.order_by('-created')[:8]
        serializer_class = ProductSer(queryset, many = True, context={'request': request})
        return Response(serializer_class.data)



class similarProduct(APIView):
    permission_classes = [permissions.AllowAny,]

    def post(self, request):
        s = idSer(data=request.data)
        if s.is_valid():
            product = Product.objects.get(id=s.validated_data['id'])
            queryset = Product.objects.filter(category = product.category).exclude(id = product.id)[:5]
            serializer_class = ProductSer(queryset, many = True, context={'request': request})
            return Response(serializer_class.data)
        else:
            return Response(s.errors)


class toOrder(APIView):
    permission_classes = [permissions.AllowAny,]

    def post(self, request):
        s = orderSer(data=request.data)
        if s.is_valid():
            l = s.validated_data['products']
            o = Order.objects.create(name = s.validated_data['name'], phone=s.validated_data['phone'])
            for i in l:
                p = Product.objects.get(id=i['id'])
                p.count_rec += 1
                p.save()
                OrderProduct.objects.create(
                    order = o,
                    product = p,
                    count = i['count']
                )
            return Response({'status': 'ok'})
        else:
            return Response(s.errors)


class getBrands(APIView):
    permission_classes = [permissions.AllowAny,]

    def get(self, request):
        brand = Brand.objects.all()
        s = []
        for i in brand:
            l = len(Product.objects.filter(brand=i))
            s.append({'id': i.id, 'name': i.name, 'count': l})
        return Response(s)
